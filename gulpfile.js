const gulp = require("gulp");
const sass = require("gulp-sass");
const sourcemaps   = require('gulp-sourcemaps');
const browserSync  = require('browser-sync');
const autoprefixer = require('gulp-autoprefixer');
const plumber      = require('gulp-plumber');
const mainfolder   = 'app';

function styles() {
  return gulp.src(mainfolder + "/assets/sass/**/[^_]*.{sass,scss}")
    .pipe(plumber({
      errorHandler: function (err) {
        console.log(err);
        this.emit('end');
      }
    }))
    .pipe(sourcemaps.init({loadMaps: true})) //sourcemaps init
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) //Стиль преобразования
    .pipe(autoprefixer(["last 10 version", "IE 10", "IE 11"], { cascade: false })) // Создаем префиксы
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(mainfolder +'/assets/css'))
}

function server() {
  browserSync.init({
    server: {
      baseDir: mainfolder
    },
    open: false,
    notify: false,
    port: 9000,
  });

  browserSync.watch([mainfolder + "/**/*.*", mainfolder + "!/**/*.css"], browserSync.reload);
}

function watch() {
  gulp.watch(mainfolder + "/assets/sass/**/*.**", styles);
}

const defaultTask = gulp.series(styles, gulp.parallel(server, watch));

exports.sass = styles;
exports.default = defaultTask;