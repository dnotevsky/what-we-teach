$(function () {
  var OPTIONS = {};
  main(OPTIONS);
  
});

function main(opts) {
  yourJourneySwiper();
}

function yourJourneySwiper() {
  if ($(window).width() > 768) {
    var mySwiper = new Swiper('.your-journey__swiper .swiper-container', {
      slidesPerView: 1,
      spaceBetween: 20,
      delay: 1500,
      speed: 0,
      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
    
      navigation: {
        nextEl: '.your-journey__swiper-button-next',
        prevEl: '.your-journey__swiper-button-prev',
      },

      on: {
        slideChange: function (swiper) {
          
            $(".your-journey__swiper-step-num").fadeOut(function() {
              setTimeout(function() {
                $(".your-journey__swiper-step-num").text(mySwiper.activeIndex+1)
                $(".your-journey__swiper-step-num").fadeIn(1000);
              }, 500)
            })
          
          if (mySwiper.previousIndex < mySwiper.activeIndex) {
            $(".your-journey__swiper").attr("data-slide", "left-" + (mySwiper.activeIndex + 1));
            setTimeout(function() {
              $(".your-journey__swiper").attr("data-slide", "top-" + (mySwiper.activeIndex + 1));
            }, 500)
          } else {
            $(".your-journey__swiper").attr("data-slide", "left-" + (mySwiper.previousIndex + 1));
            setTimeout(function() {
              $(".your-journey__swiper").attr("data-slide", "top-" + (mySwiper.activeIndex + 1));
            }, 500)
          }
          
          
        },
      },
    });    
  }
}

